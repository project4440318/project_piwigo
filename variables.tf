variable "service_account_auth_key" {
  description = "SA key in JSON for access to YAndex.Cloud"
  default = {
    id                 = ""
    service_account_id = ""
    created_at         = ""
    key_algorithm      = ""
    public_key         = ""
    private_key        = ""
  }
  type = object({
    id                 = string
    service_account_id = string
    created_at         = string
    key_algorithm      = string
    public_key         = string
    private_key        = string
    }
  )
}

variable "instances" {
  type = map(string)
  default = {
    web-01 = "1"
    db-01  = "2"
    db-02  = "3"
    db-03  = "4"
    mon-01 = "5"
  }
}

