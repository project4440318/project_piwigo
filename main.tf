terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.97.0"
    }
  }

}
provider "yandex" {
  service_account_key_file = jsonencode(var.service_account_auth_key)
  cloud_id                 = local.cloud_id
  folder_id                = local.cloud_folder_id
  zone                     = local.default_region
}
