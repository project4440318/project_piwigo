locals {
  cloud_id        = "b1gduanklkhqurtk0303" # cloud-grafev
  cloud_folder_id = "b1ghu41qau2o57ues0t1" #default

  default_region       = "ru-central1-a"
  region_ru_central1_a = "ru-central1-a"
  region_ru_central1_b = "ru-central1-b"

  compute_image = "ubuntu-2204-lts"
  platform_id   = "standard-v3" # Intel Ice Lake

  cores         = 2
  memory        = 8
  core_fraction = 50 #Уровни производительности vCPU

  v4_cidr_blocks = ["192.168.15.0/24"]


  #generate ansible inventory
  inventory_names  = values(yandex_compute_instance.vm-piwigo)[*].name
  inventory_addres = values(yandex_compute_instance.vm-piwigo)[*].network_interface.0.nat_ip_address
  inventory_user   = "graf0081"

}

