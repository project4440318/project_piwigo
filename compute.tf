data "yandex_compute_image" "ubuntu_image" {
  family = local.compute_image
}

resource "yandex_compute_instance" "vm-piwigo" {
  for_each    = var.instances
  name        = each.key
  hostname    = each.key
  platform_id = local.platform_id

  resources {
    cores         = local.cores
    memory        = local.memory
    core_fraction = local.core_fraction
  }

  boot_disk {
    initialize_params {
      size = 16
      #      type     = "network-hdd"
      image_id = data.yandex_compute_image.ubuntu_image.id
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet_piwigo.id
    nat       = true
    #    nat_ip_address = yandex_vpc_address.addr[each.key].external_ipv4_address.0.address
  }

  metadata = {
    user-data = "${file("./cloud-init/super-user.yml")}"
  }

  scheduling_policy {
    preemptible = true
  }

}

resource "yandex_vpc_network" "network_piwigo" {
  name = "net_piwigo"
}

resource "yandex_vpc_subnet" "subnet_piwigo" {
  name           = "sub_piwigo"
  zone           = local.default_region
  network_id     = yandex_vpc_network.network_piwigo.id
  v4_cidr_blocks = local.v4_cidr_blocks
}

#resource "yandex_vpc_address" "addr" {
#  for_each = var.instances
#  name     = "example-${each.key}"
#
#  #  external_ipv4_address {
#  #    zone_id = local.default_region
#  #  }
#}

resource "local_file" "generate_inventory" {
  content = templatefile("inventory_hosts.tpl", {
    names  = local.inventory_names
    addres = local.inventory_addres
    user   = local.inventory_user
  })
  filename = "./ansible/inventory/hosts"

  provisioner "local-exec" {
    when       = destroy
    command    = "mv ./ansible/inventory/hosts ./ansible/inventory/hosts.backup"
    on_failure = continue
  }
  depends_on = [yandex_vpc_subnet.subnet_piwigo]
}

resource "null_resource" "deploy" {
  provisioner "local-exec" {
    command = <<-EOT
    sleep 60 &&
    export ANSIBLE_FORCE_COLOR="true" &&
    ansible-galaxy install --force -r ./ansible/requirements.yml -p ./ansible/roles &&
    ansible-playbook ./ansible/install-piwigo.yml -i ./ansible/inventory/hosts
  EOT  
  }
  #  triggers = {
  #    addres = local.inventory_addres
  #  }
  depends_on = [local_file.generate_inventory]
}
