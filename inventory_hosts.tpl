[web]
%{for i in range(length(names)) ~}
%{if split("-", names[i])[0] == "web" ~}
${names[i]} ansible_host=${addres[i]} ansible_user=${user}
%{ endif ~}
%{ endfor ~}

[db]
%{for i in range(length(names)) ~}
%{if split("-", names[i])[0] == "db" ~}
${names[i]} ansible_host=${addres[i]} ansible_user=${user}
%{ endif ~}
%{ endfor ~}

[mon]
%{for i in range(length(names)) ~}
%{if split("-", names[i])[0] == "mon" ~}
${names[i]} ansible_host=${addres[i]} ansible_user=${user}
%{ endif ~}
%{ endfor ~}